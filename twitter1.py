# Imports
import tweepy
import boto3

comprehend = boto3.client('comprehend')

def lambda_handler(event, context):
    consumer_key = ''
    consumer_secret = ''

    access_token = ''
    access_token_secret = ''

    # auth variables
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    s =""
    topic = str(event['keyword'])
    api = tweepy.API(auth)

    public_tweets = api.search(str(topic))

    for tweet in public_tweets:
        print(tweet.text)
        s += str(tweet.text)
    
    sentiment = comprehend.detect_sentiment(Text=s, LanguageCode='en')
    words = comprehend.detect_key_phrases(Text=s, LanguageCode='en')
    
    return(s, sentiment, words)