import praw
import boto3

comprehend = boto3.client('comprehend')

def lambda_handler(event, context):

    reddit = praw.Reddit(user_agent='Post Extraction ()', client_id='', client_secret="", username='', password='')
                     
    n = 0
    s = ""
    topic = str(event['keyword'])
    appended_data = []

    subreddit = reddit.subreddit(topic)

    top_python = subreddit.top(limit=10)
    
    for submission in top_python:
        if not submission.stickied:
            appended_data.append(submission.selftext)

    while n < len(appended_data):
        # print(appended_data[n])
        s = s + appended_data[n]
        n = n + 1
    
    sentiment = comprehend.detect_sentiment(Text=str(s), LanguageCode='en')
    words = comprehend.detect_key_phrases(Text=str(s), LanguageCode='en')
    
    return(s, sentiment, words)
    print(s)
